package com.example.examobc1_serrano;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class CuentaBancoActivity extends AppCompatActivity {
    private TextView strBanco;
    private TextView lblNombre;
    private TextView lblSaldo;
    private TextView lblMovimientos;
    private EditText txtCantidad;
    private Button btnDeposito;
    private Button btnRetiro;
    private Button btnRegresar;
    private CuentaBanco cuenta;
   


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);
        iniciarComponentes();

        // Obtener los datos del MainActivity
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        String numCuenta = datos.getString("numCuenta");
        String banco = datos.getString("banco");
        String saldo = datos.getString("saldo");
        cuenta=new CuentaBanco(Integer.parseInt(numCuenta),nombre,banco,Float.parseFloat(saldo));
        // Actualizar los componentes de la interfaz de usuario con los datos recibidos
        strBanco.setText(banco);
        lblNombre.setText(nombre);
        lblSaldo.setText(saldo);
        btnDeposito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    deposito();
                } else {
                    mostrarToast("Por favor, completa todos los campos");
                }
            }
        });

        btnRetiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    retiro();
                } else {
                    mostrarToast("Por favor, completa todos los campos");
                }
            }
        });



    }

    private void iniciarComponentes() {
        strBanco = findViewById(R.id.strBanco);
        lblNombre = findViewById(R.id.lblNombre);
        lblSaldo = findViewById(R.id.lblSaldo);
        lblMovimientos = findViewById(R.id.lblMovimientos);
        txtCantidad = findViewById(R.id.txtCantidad);
        btnDeposito = findViewById(R.id.btnDeposito);
        btnRetiro = findViewById(R.id.btnRetiro);
        btnRegresar = findViewById(R.id.btnRegresar);
    }

    private boolean validarCampos() {
        String cantidad = txtCantidad.getText().toString();
        return !cantidad.isEmpty();
    }

    public void deposito() {

            float cantidad = Float.parseFloat(txtCantidad.getText().toString());
            float saldoActual = Float.parseFloat(lblSaldo.getText().toString());
            float nuevoSaldo = saldoActual + cantidad;
            cuenta.depositar(cantidad);

            lblSaldo.setText(Float.toString(cuenta.getSaldo()));
            txtCantidad.setText("");
            mostrarToast("Depósito realizado correctamente");
    }

    public void retiro() {
        float cantidad = Float.parseFloat(txtCantidad.getText().toString());
        float saldoActual = Float.parseFloat(lblSaldo.getText().toString());

            if (cantidad > saldoActual || txtCantidad.getText().toString()=="") {
                mostrarToast("No hay suficiente saldo para realizar el retiro");
            } else {
                float nuevoSaldo = saldoActual - cantidad;
                cuenta.retirar(cantidad);
                lblSaldo.setText(Float.toString(cuenta.getSaldo()));
                txtCantidad.setText("");
                mostrarToast("Retiro realizado correctamente");
            }


    }



    public void regresar(View v) {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Banco");
        confirmar.setMessage("Regresar al MainActivity?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmar.show();
    }

    private void mostrarToast(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }
}