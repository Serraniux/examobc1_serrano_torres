package com.example.examobc1_serrano;

import java.util.ArrayList;
import java.util.List;

public class CuentaBanco {
    private int numCuenta;
    private String nombre;
    private String banco;
    private float saldo;


    public CuentaBanco(int numCuenta, String nombre, String banco, float saldo) {
        this.numCuenta = numCuenta;
        this.nombre = nombre;
        this.banco = banco;
        this.saldo = saldo;

    }

    public void depositar(float cantidad) {
        saldo += cantidad;

    }

    public boolean retirar(float cantidad) {
        if (saldo >= cantidad) {
            saldo -= cantidad;

            return true;
        } else {
            return false;
        }
    }



    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
}