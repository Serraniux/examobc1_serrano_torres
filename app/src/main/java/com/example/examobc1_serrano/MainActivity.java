package com.example.examobc1_serrano;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {
    private TextInputEditText txtNumCuenta;
    private TextInputEditText txtNombre;
    private TextInputEditText txtBanco;
    private TextInputEditText txtSaldo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();


    }

    private void iniciarComponentes() {
        txtNumCuenta = findViewById(R.id.txtNumCuenta);
        txtNombre = findViewById(R.id.txtNombre);
        txtBanco = findViewById(R.id.txtBanco);
        txtSaldo = findViewById(R.id.txtSaldo);
    }

    public void btnEnviar(View v) {
        String strBanco = txtBanco.getText().toString();
        String txtNumCuentaValue = txtNumCuenta.getText().toString();
        String txtNombreValue = txtNombre.getText().toString();
        String txtSaldoValue = txtSaldo.getText().toString();

        if (txtNumCuentaValue.isEmpty() || txtNombreValue.isEmpty() || txtSaldoValue.isEmpty() || strBanco.isEmpty()) {
            // Code block executed when any field is empty or the bank is not valid
            Toast.makeText(getApplicationContext(), "Falta información en algunos campos o el banco no es válido", Toast.LENGTH_SHORT).show();
        } else {
            // Code block executed when all fields are filled and the bank is valid
            Bundle bundle = new Bundle();
            bundle.putString("numCuenta", txtNumCuentaValue);
            bundle.putString("nombre", txtNombreValue);
            bundle.putString("banco", strBanco);
            bundle.putString("saldo", txtSaldoValue);

            Intent intent = new Intent(this, CuentaBancoActivity.class);
            intent.putExtras(bundle);

            startActivity(intent);
        }
    }

    public void btnSalir(View v) {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Banco");
        confirmar.setMessage("Cerrar la aplicación?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmar.show();
    }
}